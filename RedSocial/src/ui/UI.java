package ui;

import java.io.*;
import java.util.*;
import data.Etiqueta;
import data.*;
import dao.Dao;
import servicio.Servicio;
import exceptions.NombreException;
import data.Comentario;

public class UI {
	
	private Scanner sc;
	private Servicio servicio;
	private Usuario usuario;
	private BufferedReader br;
	private boolean esRegistrado;
	private ArrayList<Foto> fotos;
	private ArrayList<Comentario> comentarios;
	private Dao dao;
 	
	public UI(){
		this.sc = new Scanner(System.in);
		this.br = new BufferedReader(new InputStreamReader(System.in));
		this.fotos = new ArrayList<Foto>();
		this.comentarios = new ArrayList<Comentario>();
		this.dao = new Dao();
	}
	
	public boolean identificacion(){
		this.usuario = this.servicio.buscarUsuarios();
		if (this.usuario != null){
			return true;
		}else{
			return false;
		}
	}
	
	public void imprimirFotos(ArrayList<Foto> fotos){
		for(Foto foto:fotos){
			System.out.println("El usuario que public� la foto es:" + foto.getUsuario().toString());
			System.out.println(foto.toString());
		}
	}
	
	public void menu() throws IOException{
		boolean reiniciar = true;
		while (reiniciar==true){
			int opcion = 0; 
        	System.out.println("Opciones: 1. Crear una red social 2.Registrar usuarios 3.Iniciar sesi�n 4.Publicar comentarios 5.Publicar fotos "
        			+ "6.Buscar usuarios 7.Listar comentarios 8. Listar las fotos 9. Buscar comentarios");
        	opcion = this.sc.nextInt();
        	if(opcion==1){
        		this.servicio = new Servicio();
        		this.dao.saveData(this.servicio.getUsuarios());
        		break;
        	}else if(opcion==2){
        		try{
        			this.usuario = this.servicio.crearUsuario();	
        		}catch(NombreException e){
        			System.out.println(e.getMessage());
        		}
        		break;
        	}else if(opcion==3){
        		this.esRegistrado = this.identificacion();
        		break;
        	}else if(opcion==4){
        		if (this.esRegistrado==true){
        			System.out.println("Escriba el texto del comentario que desea hacer");
            		String texto = this.br.readLine();
            		Date fecha= new Date();
            		Comentario comentario = new Comentario(texto,fecha,this.usuario);
            		this.usuario.getComentarios().add(comentario);
            		this.comentarios.add(comentario);
        		}else{
        			System.out.println("El usuario no se encuentra registrado");
        		}
        		break;
        	}else if(opcion==5){
        		if(this.esRegistrado==true){
        			System.out.println("Escriba el nombre de la foto:");
        			String nombreArchivo = this.sc.next();
        			System.out.println("Escriba una breve descripci�n de la foto:");
        			String descripcion = this.br.readLine();
        			Foto foto = new Foto(descripcion,nombreArchivo,this.usuario);
        			System.out.println("Ingrese el n�mero de etiquetas que desea realizar:");
        			int numero = this.sc.nextInt();
        			for(int i=0;i<numero;i++){
        				Etiqueta etiqueta = this.servicio.realizarEtiqueta();
        				foto.getEtiquetas().add(etiqueta);
        			}
        			this.usuario.getFotos().add(foto);
        			this.fotos.add(foto);
        		}else{
        			System.out.println("El usuario no se encuentra registardo.");
        		}
        		break;
        	}else if(opcion==6){
        		this.usuario = this.servicio.buscarUsuarios();
        		System.out.println(this.usuario.toString());
        		break;
        	}else if(opcion==7){
        		if(this.esRegistrado==true){
        			ArrayList<Comentario> comentarios = this.usuario.getComentarios();
        			for(Comentario comentario: comentarios){
        				System.out.println(comentario.toString());
        			}
        		}else{
        			System.out.println("El usuario no est� registrado.");
        		}
        		break;
        	}else if(opcion==8){
        		if(esRegistrado==true){
        			ArrayList<Foto> fotosEtiquetadas = this.servicio.buscarEtiquetaciones(this.fotos, this.usuario);
        			this.imprimirFotos(fotosEtiquetadas);
        		}else{
        			System.out.println("El usuario no est� registrado.");
        		}
        		break;
        	}else if(opcion==9){
        		System.out.println("Ingrese la palabra con la realizar� la b�squeda:");
        		String palabra = this.sc.next();
        		ArrayList<Comentario> comentarios = this.servicio.buscarComentarios(this.comentarios, palabra);
        		for (Comentario comentario: comentarios){
        			System.out.println(comentario.toString());
        		}
        		break;
        	}else{
        		System.out.println("Opcion incorrecta, por favor introduzca una v�lida.");
        	}
		}
	}
	
}
