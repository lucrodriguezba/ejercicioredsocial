package dao;

import java.util.*;
import java.io.*;
import data.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Dao {
	
	public void saveData(ArrayList<Usuario> usuarios){
        try {
        	FileOutputStream outStream  = new FileOutputStream("data.txt");
            ObjectOutputStream out = new ObjectOutputStream(outStream);
            for(Usuario u: usuarios){
                out.writeObject(u);
            }
        } catch (IOException ex) {
            System.out.println("ERROR");
        }
    }
	
    public ArrayList<Usuario> loadData(){
        try {
            FileInputStream inStream  = new FileInputStream("data.txt");
            ObjectInputStream in = new ObjectInputStream(inStream);
            ArrayList<Usuario> usuarios = new ArrayList<>();
            while(in.read()!=-1){
                usuarios.add((Usuario)in.readObject());
            }
            return usuarios;
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("ERROR");
        }
        return null;
    }

}
