package servicio;

import java.util.*;
import data.*;
import exceptions.NombreException;

public class Servicio {
	
	private Scanner sc;
	private ArrayList<Usuario> usuarios;
	
	public Servicio(){
		this.sc = new Scanner(System.in);
		this.usuarios = new ArrayList<Usuario>();
	}
	
	public Usuario crearUsuario() throws NombreException{
        System.out.println("Ingrese el nombre del nuevo usuario");
        String nombre = this.sc.next();
        System.out.println("Ingrese el apellido del nuevo usuario");
        String apellido = this.sc.next();
        String nombreCompleto = nombre +" "+apellido;
        if(nombreCompleto.length()>100){
            throw new NombreException("El nombre completo del usuario no puede tener m�s de 100 caracteres");
        }
        System.out.println("Ingrese el nick del nuevo usuario");
        String nick = this.sc.next();
        System.out.println("Ingrese la edad del nuevo usuario");
        int edad = this.sc.nextInt();
        System.out.println("Ingrese la clave del nuevo usuario");
        String clave = this.sc.next();
        System.out.println("Ingrese el correo del nuevo usuario");
        String correo = this.sc.next();
        Usuario usuario = new Usuario(nick,nombre,apellido,clave,correo,edad);
        this.usuarios.add(usuario);
        return usuario;
    }
	
	public ArrayList<Usuario> getUsuarios(){
		return this.usuarios;
	}
	
	public Usuario buscarUsuarios(){
		System.out.println("Ingrese el nombre del usuario:");
		String nombre = this.sc.next();
		System.out.println("Ingrese el apellido del usuario:");
		String apellido = this.sc.next();
		System.out.println("Ingrese el nick del usuario:");
		String nick = this.sc.next();
		for(Usuario usuario: this.usuarios){
			if(usuario.getNombre().equals(nombre)){
				if(usuario.getApellido().equals(apellido)){
					if(usuario.getNick().equals(nick)){
						return usuario;
					}
				}
			}
		}
		return null;
	}
	
	public Etiqueta realizarEtiqueta(){
		System.out.println("Ingrese la posicion x de la etiqueta:");
		double x = this.sc.nextDouble();
		System.out.println("Ingrese la posicion y de la etiqueta:");
		double y = this.sc.nextDouble();
		Usuario usuario = this.buscarUsuarios();
		Etiqueta etiqueta = new Etiqueta(x,y,usuario);
		return etiqueta;
	}
	
	public ArrayList<Foto> buscarEtiquetaciones(ArrayList<Foto> fotos, Usuario usuario){
		ArrayList<Foto> fotosEtiquetadas = new ArrayList<Foto>();
		for(Foto foto: fotos){
			for(Etiqueta etiqueta: foto.getEtiquetas()){
				if (etiqueta.getUsuario().getNombre().equals(usuario.getNombre())){
					if (etiqueta.getUsuario().getApellido().equals(usuario.getApellido())){
						if (etiqueta.getUsuario().getNick().equals(usuario.getNick())){
							fotosEtiquetadas.add(foto);
						}
					}
				}
			}
		}
		return fotosEtiquetadas;
	}
	
	public ArrayList<Comentario> buscarComentarios(ArrayList<Comentario> comentarios,String palabra){
		char[] word = palabra.toCharArray();
		int contador =0;
		int j = -1;
		ArrayList<Comentario> Comentarios = new ArrayList<Comentario>();
		for(Comentario comentario: comentarios){
			char[] palabras = comentario.getTexto().toCharArray();
			for(int i=0;i<palabras.length;i++){
				for(j+=1;j<word.length;){
					if (palabras[i]==word[j]){
						contador += 1;
						if (contador==word.length){
							Comentarios.add(comentario);
						}
						break;
					}else{
						break;
					}
				}
			}
		}
		return Comentarios;
	}
	
}
