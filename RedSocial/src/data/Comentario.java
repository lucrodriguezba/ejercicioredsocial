package data;

import java.util.Date;
import java.io.Serializable;

public class Comentario implements Serializable {
	
	private String texto;
    private Date date;
    private Usuario propietario;

    public Comentario(String texto, Date date, Usuario propietario) {
        this.texto = texto;
        this.date = date;
        this.propietario = propietario;
    }

    public String getTexto() {
        return texto;
    }

    @Override
    public String toString() {
        return "Comentario{" + "texto=" + texto + ", date=" + date + ", propietario=" + propietario + '}';
    }
    
}
