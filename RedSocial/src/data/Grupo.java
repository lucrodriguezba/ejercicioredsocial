package data;

import java.util.ArrayList;
import java.io.Serializable;

public class Grupo implements Serializable{

	private String descripcion;
    private String tipo;
    private String ultimasNoticias;
    private ArrayList<Usuario> miembros=new ArrayList<Usuario>();
    private Usuario propietario;

    public Grupo(Usuario propietario) {
        this.propietario = propietario;
    }
    
    public ArrayList<Usuario> addMiembro(Usuario usuario){
        miembros.add(usuario);
        return miembros;
    }
    
    public void getMiembros(){
        for (int i = 0; i < miembros.size(); i++) {
        	Usuario value = miembros.get(i);
        	System.out.println("Miembro: " + value);
        }
    }
     
}
