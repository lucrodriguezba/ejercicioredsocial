package data;

import java.util.*;
import java.io.Serializable;

public class Usuario implements Serializable{
	
	 public String nick;
	 private String nombre;
	 private String apellido;
	 private String clave;
	 private String correo;
	 private int edad;
	 private ArrayList<Comentario> comentarios;
	 private ArrayList<Grupo> grupos;
	 private ArrayList<Etiqueta> etiquetas;
	 private ArrayList<Foto> fotos;
	    
	 public Usuario(String nick, String nombre, String apellido, String clave, String correo, int edad) {
		this.nick = nick;
		this.nombre = nombre;
		this.apellido = apellido;
		this.clave = clave;
		this.correo = correo;
		this.edad = edad;
		this.comentarios = new ArrayList<Comentario>();
		this.grupos = new ArrayList<Grupo>();
		this.etiquetas = new ArrayList<Etiqueta>();
		this.fotos = new ArrayList<Foto>();
	 }

	 public String getNombre() {
		 return nombre;
	 }
	 
	 public String getNick() {
		return nick;
	 }

	 public String getApellido() {
		return apellido;
	 }

	 public ArrayList<Comentario> getComentarios() {
		 return comentarios;
	 }

	 public ArrayList<Foto> getFotos() {
		 return fotos;
	 }
	      
	 public ArrayList<Grupo> getGrupos() {
		 return grupos;
	 }

	 public ArrayList<Etiqueta> getEtiquetas() {
		 return etiquetas;
	 }
	    
	 public void PublicarFoto ( Foto foto ){
		 this.fotos.add(foto);
	 }
	 
	 public void PublicarComentario ( Comentario comentario ){
		 this.comentarios.add(comentario);
	 }

	@Override
	public String toString() {
		return "Usuario [nick=" + this.nick + ", nombre=" + this.nombre + ", apellido=" + this.apellido + ", clave=" + this.clave
				+ ", correo=" + this.correo + ", edad=" + this.edad + "]";
	}

	 
	         
}
