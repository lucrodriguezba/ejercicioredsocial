package data;

import java.util.ArrayList;
import java.io.Serializable;

public class Foto implements Serializable{
	
	private String descripcion;
    private String nombreArchivo;
    private ArrayList<Etiqueta> etiquetas;
    private Usuario usuario;
    
    public Foto(String nombreArchivo, String descripcion, Usuario usuario) {
        this.descripcion = descripcion;
        this.nombreArchivo = nombreArchivo;
        this.etiquetas = new ArrayList<Etiqueta>();
        this.usuario = usuario;
    }
    
    public ArrayList<Etiqueta> getEtiquetas() {
        return this.etiquetas;
    }
    
    public Usuario getUsuario(){
    	return this.usuario;
    }
    
    @Override
    public String toString() {  
        String nombre = "Nombre del archivo: " + this.nombreArchivo + "\n";
        String descripcion = "Descripción: " + this.descripcion + "\n";
        String string = nombre + descripcion + "Personas que aparecen:\n\n";
        for(Etiqueta marco : this.etiquetas){
            string += marco.toString() + "\n";
        }
        return string;
    }
    
}
