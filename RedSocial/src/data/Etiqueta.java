package data;

import java.util.ArrayList;
import java.io.Serializable;

public class Etiqueta implements Serializable{

	 private double x;
	 private double y;
	 private Usuario etiquetado;

	 public Etiqueta(double x, double y, Usuario usuario) {
		 this.x = x;
		 this.y = y;
		 this.etiquetado = usuario;
	 }

	 public Usuario getUsuario() {
		 return this.etiquetado;
	 }

	 @Override
	 public String toString() {
		 return "Etiqueta{" + "Coordenada x=" + this.x + ", Coordenada y=" + this.y + ", usuario que aparece etiquetado en las coordenadas=" + this.etiquetado + '}';
	 }
	    
}
